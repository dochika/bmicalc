package com.example.bmicalculator.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.bmicalculator.R
import com.google.firebase.auth.FirebaseAuth

class LoginFragment : Fragment(R.layout.fragment_login) {
    private lateinit var register1: Button
    private lateinit var button: Button
    private lateinit var password: EditText
    private lateinit var Email: EditText
    private lateinit var auth: FirebaseAuth

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        register1 = view.findViewById(R.id.register1)
        button = view.findViewById(R.id.button)
        password = view.findViewById(R.id.password)
        Email = view.findViewById(R.id.Email)
        register1.setOnClickListener {
            val action = LoginFragmentDirections.actionLoginFragmentToRegistrationFragment()
            findNavController().navigate(action)
        }
        button.setOnClickListener {
            if (password.text.toString().isEmpty()) {
                password.error = "შეიყვანეთ პაროლი"
                password.requestFocus()
                return@setOnClickListener
            }

            if (Email.text.isEmpty() && password.text.isEmpty()) {
                Toast.makeText(activity, "შეიყვანეთ თქვენი მონაცემები", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(Email.text.toString(), password.text.toString())
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val action =
                            LoginFragmentDirections.actionLoginFragmentToCalculationFragment()
                        findNavController().navigate(action)
                    } else {
                        Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
                    }
                }


        }


    }
}