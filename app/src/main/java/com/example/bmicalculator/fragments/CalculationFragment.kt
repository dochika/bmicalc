package com.example.bmicalculator.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.bmicalculator.R
import com.example.bmicalculator.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlin.math.sqrt

class CalculationFragment : Fragment(R.layout.fragment_calculation) {
    private lateinit var height: EditText
    private lateinit var weight: EditText
    private lateinit var button: Button
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_calculation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (FirebaseAuth.getInstance().currentUser == null){
            findNavController().navigate(CalculationFragmentDirections.actionCalculationFragmentToLoginFragment())
        }
        height = view.findViewById(R.id.editTextHeight)
        weight = view.findViewById(R.id.editTextWeight)
        button = view.findViewById(R.id.button3)
        button.setOnClickListener {
            if (height.text.toString().isNullOrEmpty() && weight.text.toString().isNullOrEmpty()) {
                Toast.makeText(activity, "Please Enter Your Info", Toast.LENGTH_SHORT).show()

            } else {
                val Height = height.text.toString().toDouble()
                val Weight = weight.text.toString().toDouble()
                //  mass(kg) / height2(m)
                val calculation = (Weight / sqrt(Height / 10)).toBigDecimal().toString()
                findNavController().navigate(
                    CalculationFragmentDirections.actionCalculationFragmentToResultFragment(
                        calculation
                    )
                )
                val ref = FirebaseDatabase.getInstance().getReference("UserInfo").push()
                ref.setValue(UserInfo(title = calculation , uid = FirebaseAuth.getInstance().uid.toString())).addOnSuccessListener {
                    Toast.makeText(activity, "data added", Toast.LENGTH_SHORT).show()
                }

            }
        }

    }
}