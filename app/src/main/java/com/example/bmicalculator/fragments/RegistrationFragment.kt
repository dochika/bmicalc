package com.example.bmicalculator.fragments

import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.bmicalculator.R
import com.google.firebase.auth.FirebaseAuth

class RegistrationFragment : Fragment(R.layout.fragment_signup) {
    private lateinit var auth: FirebaseAuth
    private lateinit var Email: EditText
    private lateinit var password: EditText
    private lateinit var button: Button
    private lateinit var login: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auth = FirebaseAuth.getInstance()

        Email = view.findViewById(R.id.Email)
        password = view.findViewById(R.id.password)
        button = view.findViewById(R.id.button)
        login = view.findViewById(R.id.login)

        login.setOnClickListener{
            val action = RegistrationFragmentDirections.actionRegistrationFragmentToLoginFragment()
            findNavController().navigate(action)
        }

        button.setOnClickListener {
            signUpUser()
            return@setOnClickListener
        }
    }

    private fun signUpUser() {

        if (Email.text.toString().isEmpty()) {
            Email.error = "შეიყვანეთ ემაილი"
            Email.requestFocus()
        }
        if (Patterns.EMAIL_ADDRESS.matcher(Email.text.toString()).matches()) {
            Email.error = "ემაილი გამოყენებულია ან არ არის ნამდვილი"
            Email.requestFocus()
        }
        if (password.text.toString().isEmpty()) {
            password.error = "შეიყვანეთ პაროლი"
            password.requestFocus()
        }

        FirebaseAuth.getInstance()
            .createUserWithEmailAndPassword(Email.text.toString(), password.text.toString())
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val action =
                        RegistrationFragmentDirections.actionRegistrationFragmentToLoginFragment()
                    findNavController().navigate(action)
                } else {
                    Toast.makeText(activity, "error", Toast.LENGTH_SHORT).show()

                }
            }

    }
}