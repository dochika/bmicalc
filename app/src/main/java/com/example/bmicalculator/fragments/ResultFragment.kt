package com.example.bmicalculator.fragments

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bmicalculator.R
import com.example.bmicalculator.RecyclerAdapter
import com.example.bmicalculator.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ResultFragment : Fragment(R.layout.fragment_answer) {
    private lateinit var recyclerView: RecyclerView
    private lateinit var textView: TextView
    private lateinit var userArrayList: ArrayList<UserInfo>
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        userArrayList = arrayListOf<UserInfo>()
        getUserData()
        textView = view.findViewById(R.id.RESULT)
        textView.text = ResultFragmentArgs.fromBundle(requireArguments()).argument.toString()


    }


    private fun getUserData() {
        val ref = FirebaseDatabase.getInstance().getReference("UserInfo")
        ref.orderByChild("uid").equalTo(FirebaseAuth.getInstance().uid.toString())
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        for (userSnapshot in snapshot.children) {
                            val user = userSnapshot.getValue(UserInfo::class.java)
                            userArrayList.add(user!!)
                        }
                        recyclerView.adapter = RecyclerAdapter(userArrayList)
                    }


                }

                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

            })
    }
}