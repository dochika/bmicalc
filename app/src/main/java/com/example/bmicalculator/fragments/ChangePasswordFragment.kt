package com.example.bmicalculator.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.bmicalculator.R
import com.google.firebase.auth.FirebaseAuth

class ChangePasswordFragment : Fragment(R.layout.fragment_change_password) {
    private lateinit var Email: EditText
    private lateinit var button: Button
    private lateinit var register: TextView
    private lateinit var login: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Email = view.findViewById(R.id.Email)
        button = view.findViewById(R.id.button)
        register = view.findViewById(R.id.register)
        login = view.findViewById(R.id.login)

        button.setOnClickListener {

            if (Email.text.toString().isEmpty()) {
                Toast.makeText(activity, "შეიყვანეთ ემაილი", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().sendPasswordResetEmail(Email.text.toString())
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val action =
                            ChangePasswordFragmentDirections.actionChangePasswordFragmentToProfileFragment()
                        findNavController().navigate(action)
                        Toast.makeText(activity, "შეამოწმეთ ემაილი", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(
                            activity,
                            "ვერ გაიგზავნა,  სცადეთ თავიდან",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
        }
    }
}

