package com.example.bmicalculator.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.bmicalculator.R
import com.google.firebase.auth.FirebaseAuth

class ProfileFragment: Fragment(R.layout.fragment_profile) {
private lateinit var button:Button
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button = view.findViewById(R.id.button2)
        button.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToLoginFragment())
        }
    }
    }