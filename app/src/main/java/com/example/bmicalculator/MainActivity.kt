package com.example.bmicalculator

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val bottomNavigationMenu = findViewById<BottomNavigationView>(R.id.bottomNavMenu)

        val controller = findNavController(R.id.nav_host_fragment)
        bottomNavigationMenu.setupWithNavController(navController = controller)
        controller.addOnDestinationChangedListener{ navController: NavController, navDestination: NavDestination, bundle: Bundle? ->

            when (navDestination.id) {
                R.id.loginFragment , R.id.registrationFragment, R.id.changePasswordFragment-> bottomNavigationMenu.visibility = GONE

                else -> bottomNavigationMenu.visibility = VISIBLE

            }

        }
    }
}
