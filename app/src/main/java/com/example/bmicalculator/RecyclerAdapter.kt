package com.example.bmicalculator

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter(val userList: ArrayList<UserInfo>) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var itemTitle : TextView
        init {
            itemTitle = itemView.findViewById(R.id.itemTitle)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerAdapter.ViewHolder {
        val v =LayoutInflater.from(parent.context).inflate(R.layout.card_layout, parent , false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerAdapter.ViewHolder, position: Int) {
        val currentitem = userList[position]
        holder.itemTitle.text = currentitem.title
    }

    override fun getItemCount(): Int {
        return userList.size
    }
}
