package com.example.bmicalculator

data class UserInfo(
    val title: String = "",
    val added: String = "",
    val uid : String = ""
)
